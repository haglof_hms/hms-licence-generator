<!-- #INCLUDE FILE="SHA256.inc" -->

<%
	Sub MakeConn(Conn)
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open = "Provider=SQLOLEDB;Server=oden;Database=hms_licence;User Id=sa;Pwd=sa;"
	End Sub

	Sub MakeRs_add(Rs,Conn,sql)
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open sql, Conn, adOpenStatic, adLockOptimistic, adCmdText
	End Sub

	Sub MakeRs_add2(Rs,Conn,sql)
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.CursorLocation = adUseClient 'This will allow Resync
		Rs.Open sql, Conn, adOpenStatic, adLockPessimistic, adCmdText
	End Sub

	Sub MakeRs_view(Rs,Conn,sql)
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open sql, Conn, adOpenStatic, adLockReadOnly, adCmdText
	End Sub

	Sub Destroy(Name)
		Name.Close
		Set Name = Nothing
	End Sub

	Function FilterInput(input)
		'filter out any malicious characters to prevent SQL-injection
		input = Replace(input, "'", "")
		input = Replace(input, "%", "")
		'input = Replace(input, ":", "")
		'input = Replace(input, "--", "")

		FilterInput = input
	End Function

	Function pwdencrypt(password)
		Dim salt
		salt = "8dfe09"
		pwdencrypt = sha256(salt + password)
	End Function

	Function CheckADLogin(user, pwd, group, ByRef error)
		Dim adoCommand, adoConnection, adoRecordset
		Dim oConn, openDS, objRootDSE
		Dim strUser, strPassword, strDNSDomain, strBase, strFilter, strAttributes, strQuery

		CheckADLogin = False

		' Setup ADO objects.
		Set adoCommand = CreateObject("ADODB.Command")
		Set adoConnection = CreateObject("ADODB.Connection")
		adoConnection.Provider = "ADsDSOObject"
		adoConnection.Open "Active Directory Provider"
		adoCommand.ActiveConnection = adoConnection

		strUser = "HAGLOF\" & user
		strPassword = pwd
		Set oConn = server.CreateObject("ADODB.Connection")
		oConn.Provider = "ADsDSOObject"
		oConn.Properties("User ID") = strUser
		oConn.Properties("Password") = strPassword
		oConn.Properties("Encrypt Password") = True
		oConn.Open "DS Query", strUser, strPassword


		' Search entire Active Directory domain.
		Set openDS = GetObject("GC:")
		On Error Resume Next
		Set objRootDSE = openDS.OpenDSObject("GC://RootDSE", strUser, strPassword, &H4)
		If Err.number <> 0 Then
			error = "Invalid username or password!"
		End If
		On Error Goto 0

		If error = vbNullString Then
			strDNSDomain = objRootDSE.Get("defaultNamingContext")
			strBase = "<LDAP://" & strDNSDomain & ">"

			' Filter results.
			strFilter = "(&(objectCategory=person)(objectClass=user)(memberOf=CN=" & group & ",CN=Users,DC=HAGLOF,DC=lokal)(sAMAccountName=" & user & "))"

			' Comma delimited list of attribute values to retrieve.
			strAttributes = "sAMAccountName,cn,memberOf"

			' Construct the LDAP syntax query.
			strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"
			adoCommand.CommandText = strQuery
			adoCommand.Properties("Page Size") = 100
			adoCommand.Properties("Timeout") = 30
			adoCommand.Properties("Cache Results") = False
			'adoCommand.ActiveConnection = oConn


			' Run the query.
			Set adoRecordset = adoCommand.Execute
	         
			If adoRecordset.EOF Then
				error = "You are not a member of the group " & group & "!"
			Else
				CheckADLogin = True
			End If
	        
			adoRecordset.Close
			Set adoRecordset = Nothing
		End If


		' Clean up.
		oConn.Close
		adoConnection.Close

		Set oConn = Nothing
		Set adoConnection = Nothing
	End Function
	
	Sub DoADLogin(username, con)
		Dim rst, SQL

		Const DefaultRetailerId = 1 'Hagl�f Sweden AB

        'AD user - find matching user account, create new if not found
        SQL = "SELECT * FROM useraccount WHERE username LIKE '" & username & "'"
        Call MakeRs_add2(rst,con,SQL)
        If Not rst.Eof Then
            'User account found
            Session("retailerid") = rst("retailerid")
            Session("userid") = rst("userid")
            If rst("supervisor") = True Then Session("supervisor") = "true"
        Else
            'Create new user account
            rst.AddNew()
            rst("username") = username
            rst("supervisor") = 1
            rst("retailerid") = DefaultRetailerId
            rst.Resync()
            Session("userid") = rst("userid")
            Session("retailerid") = DefaultRetailerId
        End If
        Call Destroy(rst)
	End Sub

	Sub DownloadFile(file)
		Dim strFileExtension
		Dim objFSO
		Dim objFile
		Dim objStream

		Set objFSO = Server.CreateObject("Scripting.FileSystemObject")

		If objFSO.FileExists(file) Then
			Set objFile = objFSO.GetFile(file)

			'Clear the response and set the appropriate headers
			Response.Clear
			Response.AddHeader "Content-Disposition", "attachment; filename=" & objFile.Name
			Response.AddHeader "Content-Length", objFile.Size
			Response.ContentType = "application/octet-stream"

			Set objStream = Server.CreateObject("ADODB.Stream")
			objStream.Open
			objStream.Type = 1
			Response.CharSet = "UTF-8"

			objStream.LoadFromFile(file)
			Response.BinaryWrite(objStream.Read)
			Response.Flush	'Download did not work without this (2013-12-09)

			objStream.Close
			Set objStream = Nothing
			Set objFile = Nothing
		Else
			Response.Clear
			Response.Write("File does not exist.")
		End If

		Set objFSO = Nothing
	End Sub
%>
