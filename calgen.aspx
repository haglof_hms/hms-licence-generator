﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="calgen.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <form id="form1" runat="server">
    <div>
    <%
    int ret = exec(Request["pid"], Request["serial"], Request["level"]);
    Response.Redirect(String.Concat("caliperlic.asp?cmd=post-generate&retcode=", ret));
    %>
    </div>
    </form>
</body>
</html>
