using System;

public partial class _Default : System.Web.UI.Page
{
    protected int exec(String pid, String serial, String level)
    {
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.EnableRaisingEvents = true;
        proc.StartInfo.WorkingDirectory = "C:\\Inetpub\\AdminScripts";
        proc.StartInfo.FileName = "calgen.exe";
        proc.StartInfo.Arguments = String.Concat(String.Concat(String.Concat("digitechpro -pid ", pid), " -serial ", serial), " -level ", level);
        proc.Start();
        proc.WaitForExit();
        return proc.ExitCode;
    }
}
