<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->

<% If Int(Session("retailerid")) <= 0 Then Response.Redirect "./" %>

<html>
<head>
<script language="javascript">
function ClipBoard(text) 
{
  holdtext.innerText = text;
  Copied = holdtext.createTextRange();
  Copied.execCommand("Copy");
}
</script>
</head>
<body>

<% Sub PrintLicenceHeader() %>
    <td style="width:120px"><b>Module</b></td>
    <td style="width:350px"><b>Licencekey</b></td>
    <td style="width:130px"><b>Hardware ID</b></td>
    <td style="width:130px"><b>Count</b></td>
    <td style="width:180px"><b>Order date</b></td>
    <td style="width:130px"><b>Salesman</b></td>
<% End Sub %>

<%
Dim con, rst, rst2, SQL
Dim customernum, orderid, text

orderid = FilterInput(Request("id"))
If Not IsNumeric(orderid) Or orderid = vbNullString Then Response.Redirect "licence-view.asp"

Call MakeConn(con)

If Request("cmd") = "updatecustomer" Then
    customernum = FilterInput(Request("customernum"))
    orderid = FilterInput(Request("id"))

    If Not IsNumeric(customernum) Or Not IsNumeric(orderid) Then
        Call Destroy(con)
        Response.Redirect "licence-view.asp"
    End If

    'Update customer id for specified order
    SQL = "UPDATE licenceorder SET customernum = " & customernum & " WHERE orderid = " & orderid & " AND customernum IS NULL"
    Call con.Execute(SQL)
    Call Destroy(con)

    Response.Redirect "details.asp?id=" & orderid & "&selection=" & Request("selection")
End If


'Print order details, make sure order is placed by this retailer or user is a supervisor
SQL = "SELECT orderid, hwid, username, retailername " & _
      "FROM licenceorder " & _
      "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
      "INNER JOIN retailer ON retailer.retailerid = useraccount.retailerid " & _
      "WHERE orderid = " & orderid & _
      "AND (useraccount.retailerid = " & Session("retailerid") & " OR '" & Session("supervisor") & "' = 'true')"
Call MakeRs_view(rst,con,SQL)
If rst.Eof Then
    Response.Redirect "licence-view.asp"
ElseIf Request("selection") <> "customer" Then
    %>
    <table>
    <tr><td style="width:120px"><b>Order details</b></td></tr>
    <tr><td>Orderno.</td><td><%=rst("orderid") %></td></tr>
    <tr><td>Hardware ID</td><td><%=rst("hwid") %></td></tr>
    <tr><td>Salesman</td><td><%=rst("username") %> (<%=rst("retailername") %>)</td></tr>
    </table>
    <%
    text = text & rst("orderid") & "\n" & rst("hwid") & "\n" & rst("username") & " (" & rst("retailername") & ")\n\n"
End If
Call Destroy(rst)


'Print customer details for this order
SQL = "SELECT DISTINCT licenceorder.customernum, company, notes " & _
      "FROM licenceorder " & _
      "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
      "WHERE orderid = " & orderid
Call MakeRs_view(rst,con,SQL)
If Not rst.Eof Then
    customernum = rst("customernum")
    %>
    <p></p>
    <table>
    <tr><td style="width:120px"><b>Customer details</b></td></tr>
    <tr><td>Customerno.</td><td>
    <%If IsNull(rst("customernum")) Then %>
        <form name="f" method="post" action="details.asp">
        <input type="hidden" name="cmd" value="updatecustomer" />
        <input type="hidden" name="id" value="<%=Request("id") %>" />
        <input type="hidden" name="selection" value="<%=Request("selection") %>" />
        <input type="text" name="customernum" />&nbsp;<input type="submit" value="Apply" /><br />
        <a href="#" onclick="window.open('customer-search.asp', 'customerreg', 'height=600,width=450,resizable=0,scrollbars=1')"><div style="font-size:x-small">Search customer register</div></a>
        </form>
    <%Else %>
        <%=rst("customernum") %>
    <%End If %>
    </td></tr>
    <tr><td>Company</td><td><%=rst("company") %></td></tr>
    <tr><td>Notes</td><td><%=rst("notes") %></td></tr>
    </table>
    <%
    text = text & rst("customernum") & "\n" & rst("company") & "\n\n"
End If
Call Destroy(rst)
%>

<table>
<%
'Print licence details
If Request("selection") = "hwid" Then
    %><tr style="height:50px" valign="bottom"><%Call PrintLicenceHeader()%></tr><%

    'Print all licences with the same HWID as this order
    SQL = "SELECT DISTINCT moduleid, modulename, licencekey, hwid, licencecount, demo, copyprotection, date, username " & _
          "FROM licence " & _
          "INNER JOIN licenceorder ON licenceorder.orderid = licence.orderid " & _
          "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
          "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
          "WHERE hwid IN (SELECT hwid FROM licenceorder WHERE orderid = " & orderid & ") " & _
          "ORDER BY date DESC"
    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        %>
        <tr <%If rst("moduleid") = "1" Then%>style="color:#cccccc"<%ElseIf rst("demo") = True Then %>style="color:#cc0000"<%End If%>>
        <td><%=rst("modulename") %></td>
        <td><%=rst("licencekey") %></td>
        <td><%=rst("hwid") %> (<%=rst("copyprotection") %>)</td>
        <td><%If rst("licencecount") > 0 Then %>+&nbsp;<%=rst("licencecount") %><%If rst("demo") = True Then %> days<%Else %> licences<%End If %><%End If %><%If rst("demo") = True Then %> (demo)<%End If %></td>
        <td><%=rst("date") %></td>
        <td><%=rst("username") %></td>
        </tr>
        <%
        text = text & rst("modulename") & "\t" & rst("licencekey") & "\n"
        rst.MoveNext
    Loop
    Call Destroy(rst)
Else
    'List all HWIDs for this customer and licences for each HWID
    SQL = "SELECT DISTINCT hwid, customernum FROM licenceorder WHERE customernum IN (SELECT customernum FROM licenceorder WHERE orderid = " & orderid & ")"
    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        text = text & "HWID " & rst("hwid") & "\n"
        %>
        <tr style="height:50px" valign="bottom"><td><b>Hardware ID <%=rst("hwid") %></b></td></tr>
        <%
        %><tr><%Call PrintLicenceHeader()%></tr><%
        'List all licences with the current HWID
        SQL = "SELECT DISTINCT moduleid, modulename, licencekey, hwid, licencecount, demo, copyprotection, date, username " & _
              "FROM licence " & _
              "INNER JOIN licenceorder ON licenceorder.orderid = licence.orderid " & _
              "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
              "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
              "WHERE hwid = '" & rst("hwid") & "' AND licenceorder.customernum = " & rst("customernum") & " " & _
              "ORDER BY date DESC"
        Call MakeRs_view(rst2,con,SQL)
        Do Until rst2.Eof
            %>
            <tr <%If rst2("moduleid") = "1" Then %>style="color:#cccccc"<%ElseIf rst2("demo") = True Then %>style="color:#cc0000"<%End If %>>
            <td><%=rst2("modulename") %></td>
            <td><%=rst2("licencekey") %></td>
            <td><%=rst2("hwid") %> (<%=rst2("copyprotection") %>)</td>
            <td><%If rst2("licencecount") > 0 Then %>+&nbsp;<%=rst2("licencecount") %><%If rst2("demo") = True Then %> days<%Else %> licences<%End If %><%End If %></td>
            <td><%=rst2("date") %></td>
            <td><%=rst2("username") %></td>
            </tr>
            <%
            text = text & rst2("modulename") & "\t" & rst2("licencekey") & "\n"

            rst2.MoveNext
        Loop
        Call Destroy(rst2)
        
        text = text & "\n"

        rst.MoveNext
    Loop
    Call Destroy(rst)
End If

Call Destroy(con)
%>
</table>


<textarea id="holdtext" style="display:none;"></textarea>
<p><button style="width:130px" onclick="ClipBoard('<%=text %>');">Copy to Clipboard</button></p>
<p><button style="width:130px" onclick="window.location.href='newlicence.asp?customernum=<%=customernum %>'">Create new licence</button></p>


<p><a href="./">Back to main menu</a>

</body>
</html>
