<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Session("admin") <> "true" Then Response.Redirect "admin.asp" %>

<html>
<body>

<%
Dim cmd, con, rst, SQL
Dim userid, username, retailerid, supervisor, status

cmd = Request("cmd")

Call MakeConn(con)

Select Case cmd
Case "update"
    If Request("cmd2") <> "Create" Then
        userid = FilterInput(Request("uid"))
        If Not IsNumeric(userid) Or Trim(userid) = vbNullString Then Response.Redirect "useraccounts.asp"
    End If

    Select Case Request("cmd2")
    Case "Update", "Create"
        username = FilterInput(Request.Form("username"))
        retailerid = FilterInput(Request.Form("retailerid"))

        If Request.Form("supervisor") = "on" Then
            supervisor = True
        Else
            supervisor = False
        End If

        If Request("cmd2") = "Update" Then
            'Update user data
            SQL = "UPDATE useraccount SET " & _
                  "username = '" & username & "', " & _
                  "retailerid = " & retailerid & ", " & _
                  "supervisor = '" & supervisor & "'" & _
                  "WHERE userid = " & userid
            Call con.Execute(SQL)
        Else
            'Add new user account
            SQL = "INSERT INTO useraccount (username, password, supervisor, retailerid) " & _
                  "VALUES('" & username & "', '" & pwdencrypt(FilterInput(Request.Form("pwd"))) & "', '" & supervisor & "', " & retailerid & ")"
            Call con.Execute(SQL)
        End If

        Call Destroy(con)
        Response.Redirect("useraccounts.asp")

    Case "Reset password"
        'Update user password
        If Trim(Request.Form("pwd")) <> vbNullString Then
            SQL = "UPDATE useraccount SET password = '" & pwdencrypt(Request.Form("pwd")) & "' WHERE userid = " & userid
            Call con.Execute(SQL)
            status = "pwdupdated"
        End If
        
        Call Destroy(con)
        Response.Redirect("useraccounts.asp?status=" & status)

    Case "Remove account"
        'Remove user account
        SQL = "DELETE FROM useraccount WHERE userid = " & userid
        Call con.Execute(SQL)
        Call Destroy(con)
        Response.Redirect "useraccounts.asp"
    End Select

Case "view", "new"
    If cmd <> "new" Then
        userid = FilterInput(Request("uid"))
        If Not IsNumeric(userid) Or Trim(userid) = vbNullString Then Response.Redirect "useraccounts.asp"

        'Get user data
        SQL = "SELECT username, retailerid, supervisor FROM useraccount WHERE userid = " & userid
        Call MakeRs_view(rst,con,SQL)
        If rst.Eof Then Response.Redirect "useraccounts.asp"
        username = rst("username")
    End If
    %>

    <form action="useraccounts.asp" method=post>
    <input type=hidden name="cmd" value="update">
    <input type=hidden name="uid" value="<%=userid %>">
    <table border=0>
    <tr><td style="width:100px">Username</td><td><input type=text style="width:200px" name="username" value="<%=username %>"</td></tr>

    <%
    If cmd <> "new" Then
        retailerid = rst("retailerid")
        supervisor = rst("supervisor")
        Call Destroy(rst)
    End If

    'Get list of retailers
    SQL = "SELECT * FROM retailer"
    Call MakeRs_view(rst,con,SQL)
    %>

    <tr>
    <td>Retailer</td>
    <td><select style="width:200px" name="retailerid">

    <%
    Do Until rst.Eof
        %><option value="<%=rst("retailerid") %>" <%If rst("retailerid") = retailerid Then Response.Write "selected" %>><%=rst("retailername") %></option><%
        rst.MoveNext
    Loop
    %>

    </select></td>
    </tr>
    <tr><td>Supervisor</td><td><input type=checkbox name="supervisor" <%If supervisor = True Then Response.Write "checked" %>></td></tr>
    <tr><td>New password</td><td><input type=password style="width:200px" name=pwd></td><%If cmd <> "new" Then %><td><input type=submit name="cmd2" value="Reset password"></td><%End If %></tr>
    <%If cmd <> "new" Then %><tr><td colspan=3><font size=2>To assign a new password enter the new password and press 'Reset password'</font></td></tr><%End If %>
    <tr style="height:50px"><td><input type=submit name="cmd2" value="<%If cmd <> "new" Then %>Update<%Else %>Create<%End If %>"></td><%If cmd <> "new" Then %><td colspan=2 align=right><input type=submit name="cmd2" value="Remove account"></td><%End If %></tr>
    </table>
    </form>

    <%
    Call Destroy(rst)

Case Else
    If Request("status") <> vbNullString Then
        Response.Write "<div align=center><font color=red>"
        Select Case Request("status")
        Case "pwdupdated"
            Response.Write "Password successfully updated!"
        End Select
        Response.Write "</font></div><hr>"
    End If
    %>

    <table border=0>
    <tr>
    <td style="width:200px"><b>Username</b></td>
    <td style="width:200px"><b>Retailer</b></td>
    <td style="width:80px"><b>Supervisor</b></td>
    </tr>

    <%
    'List retailers
    SQL = "SELECT userid, username, retailername, supervisor FROM useraccount INNER JOIN retailer ON retailer.retailerid = useraccount.retailerid ORDER BY retailername, username"
    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        %>
        <tr>
        <td><a href="useraccounts.asp?cmd=view&uid=<%=rst("userid") %>"><%=rst("username") %></a></td>
        <td><%=rst("retailername") %></td>
        <td><%If rst("supervisor") = True Then%><%=rst("supervisor") %><%End If%></td>
        </tr>
        <%
        rst.MoveNext
    Loop
    Call Destroy(rst)
    %>

    </table>
    
    <p>
    <a href="useraccounts.asp?cmd=new">Create new account</a>

    <%
End Select

Call Destroy(con)
%>

<p><a href="admin.asp">Back to main menu</a>

</body>
</html>
