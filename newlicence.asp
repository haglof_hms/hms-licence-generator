<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Request("cmd") = "autogen" And Session("retailerid") = vbNullString Then Session("retailerid") = "-1" %>
<% If Session("retailerid") = vbNullString Then Response.Redirect "./" %>
<% Server.ScriptTimeout = 120 %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" language="javascript">
function licfile()
{
    if( document.f.licencefile.checked == true )
    {
        // Check & disable activation key
        document.f.activationkey.checked = true;
        document.f.activationkey.disabled = true;

        // Check new licence radio button, disable extended demo
        var radioObj = document.f.type;
        if( radioObj.length != undefined )
        {
	        for( var i = 0; i < radioObj.length; i++ )
	        {
		        if( radioObj[i].value == 'newlic' )
		        {
			        radioObj[i].checked = true;
		        }
		        else if( radioObj[i].value == 'extdemo' )
		        {
		            radioObj[i].disabled = true;
		        }
	        }
	    }
    }
    else
    {
        // Enable activation key, extended demo
        document.f.activationkey.disabled = false;
        var radioObj = document.f.type;
        if( radioObj.length != undefined )
        {
	        for( var i = 0; i < radioObj.length; i++ )
	        {
		        if( radioObj[i].value == 'extdemo' )
		        {
		            radioObj[i].disabled = false;
		        }
	        }
	    }
    }
}
</script>
</head>
<body onload="licfile();">

<%
Const ForReading = 1, ForWriting = 2, ForAppending = 3
Const TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0

Dim OutFile, Ext
Dim cmd, item, num, itemCount, keypath, xmlData
Dim fso, file
Dim error, autogen, licencefile, actkey, lictype
Dim items(), licenceCount() 'item id array, licence count array (these two has to match)
Dim i, j, ct
Dim objXML, objLst
Dim id, name, tag, key, keytype, args, orderid
Dim admin, retailerid, userid, supervisor
Dim con, rst, rst2, SQL

Server.ScriptTimeout = 120 '2 minutes

cmd = Request("cmd")
actkey = Request("activationkey")
lictype = Request("type")

'Check if autogeneration
autogen = False
On Error Resume Next
autogen = CBool(Request("autogen"))
On Error Goto 0
If cmd = "autogen" Then autogen = True

'Check if licencefile is checked
licencefile = False
If Request("licencefile") = "on" Then
    licencefile = True

    'Make sure we create activation key & new licence
    actkey = "on"
    lictype = "newlic"
End If

'Indicate an error until succeeded (autogeneration only, in case USB stick utility is used)
If autogen = True Then Response.Status = "503 Service Unavailable"

Select Case cmd
Case "autogen"
    'Parse project file xml
    Set objXML = Server.CreateObject("Microsoft.XMLDOM")
    Set objLst = Server.CreateObject("Microsoft.XMLDOM")
    objXML.async = False
    objXML.Load(ProjectDir & "\" & ProjectFile)
    If objXML.parseError.errorCode <> 0 Then
        Response.Write "An error occured while trying to parse licence project file.<p>"
        Response.Write "Reason: " & objXML.parseError.reason & "<br>"
	    Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
	    Response.Write "Line: " & objXML.parseError.line & "<br>"
	    Response.Write "</p>"
        Response.End
    End If

    Set objLst = objXML.getElementsByTagName("Module")

    'Add all modules to query string
    ct = 0
    For i = 0 to (objLst.length - 1)
        id = vbNullString

        For j = 0 to objLst.item(i).childNodes.Length - 1
            Select Case objLst.item(i).childNodes(j).nodeName
            Case "ModuleNumber", "ModuleID"
                id = objLst.item(i).childNodes(j).text
            End Select
        Next

        args = args & "&module" & ct & "=on&" & "id" & ct & "=" & id

        ct = ct + 1 'Increase counter
    Next

    Set objLst = Nothing
    Set objXML = Nothing

    'Fault check
    If ct = 0 Then
        Response.Write "No modules in project file!"
        Response.End
    End If

    If Request("copyprotection") = "false" Then
        'Used to generate a demo licence file
        Response.Redirect "newlicence.asp?cmd=generate&autogen=true&copyprotection=false" & args
    Else
        'Used by auto-generator to generate an USB memory licence file
        Response.Redirect "newlicence.asp?cmd=generate&autogen=true&copyprotection=11&activationkey=on&code=" & Request("code") & args
    End If

Case "generate"
    'Check constraints
    If autogen = False Then
        If Len(Request("code")) <> 8 Then
            error = "Installation code must be exactly eight characters long."
        End If
    End If

    If error = vbNullString Then
        itemCount = 0
        Call MakeConn(con)

        If licencefile = True Then
            'Parse project file xml
            Set objXML = Server.CreateObject("Microsoft.XMLDOM")
            Set objLst = Server.CreateObject("Microsoft.XMLDOM")
            objXML.async = False
            objXML.Load(ProjectDir & "\" & ProjectFile)
            If objXML.parseError.errorCode <> 0 Then
                Response.Write "An error occured while trying to parse licence project file.<p>"
                Response.Write "Reason: " & objXML.parseError.reason & "<br>"
	            Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
	            Response.Write "Line: " & objXML.parseError.line & "<br>"
	            Response.Write "</p>"
                Response.End
            End If

            Set objLst = objXML.getElementsByTagName("Module")

            'Get all modules
            For i = 0 to (objLst.length - 1)
                id = vbNullString
                ReDim Preserve items(itemCount)
                ReDim Preserve licenceCount(itemCount)

                For j = 0 to objLst.item(i).childNodes.Length - 1
                    Select Case objLst.item(i).childNodes(j).nodeName
                    Case "ModuleNumber", "ModuleID"
                        id = objLst.item(i).childNodes(j).text
                    End Select
                Next

                'Check if this module is checked, if so get licence count
                items(itemCount) = id
                licenceCount(itemCount) = 0
                j = 0
                For Each item In Request.QueryString
                    If Request("id" & j) = id And Request("module" & j) = "on" Then
                        'Validate licence count
                        If Not IsNumeric(Request("licencecount" & id)) Then
                            error = "Invalid licence count for one or more modules."
                        Else
                            'Add one licence by default when creating a file
                            licenceCount(itemCount) = Request("licencecount" & id) + 1
                        End If

                        Exit For
                    End If

                    j = j + 1
                Next

                'Check permission for this module (this will build up an error message in case user doesn't have permission)
                If licenceCount(itemCount) <> 0 Then Call CheckPermission(items(itemCount), con)

                itemCount = itemCount + 1
            Next

            Set objLst = Nothing
            Set objXML = Nothing
        Else
            For Each item In Request.QueryString
                'List selected modules
                If left(item, 6) = "module" And Request(item) = "on" Then
                    num = mid(item, 7)
                    ReDim Preserve items(itemCount)
                    ReDim Preserve licenceCount(itemCount)
                    items(itemCount) = Request("id" & num)
                    licenceCount(itemCount) = Request("licencecount" & items(itemCount))
                    
                    'Validate licence count
                    If Not IsNumeric(Request("licencecount" & items(itemCount))) Then
                        error = "Invalid licence count for one or more modules."
                    End If

                    If autogen = False Then
                        'Check user permission for this module
                        Call CheckPermission(items(itemCount), con)
                        
                        'Check number of days if extended demo
                        If lictype = "extdemo" And IsNumeric(licenceCount(itemCount)) Then
                            If CLng(licenceCount(itemCount)) <= 0 Then
                                error = "Number of new days must be at least one for module " & items(itemCount) & "."
                            End If
                        End If
                    End If

                    itemCount = itemCount + 1
                End If
            Next
        End If
        Call Destroy(con)

        If error = vbNullString Then
            'Set up filename for generated key file
            OutFile = "hms-key_" & Replace(CStr(Timer()), ",", "") & ".xml" 'make sure this filename is unique in case of simultaneous generations

            'Generate xml files (product & order)
            If itemCount > 0 Then
                Set fso = Server.CreateObject("Scripting.FileSystemObject")

                'Product
                Set file = fso.CreateTextFile(TempDir + "\" + ProductFile, True)
                file.WriteLine("<Connector>")
                file.WriteLine("   <Product>")
                file.WriteLine("      <ProductID>1111</ProductID>")
                file.WriteLine("      <ProductName>HMS</ProductName>")
                file.WriteLine("      <ProjectFilename>" + ProjectFile + "</ProjectFilename>")
                If Request("copyprotection") <> "false" Then
                    file.WriteLine("      <CopyProtection>" & Request("copyprotection") & "</CopyProtection>")
                    If actkey = "on" Then
                        file.WriteLine("      <CreateCopyProtectionKey>Yes</CreateCopyProtectionKey>")
                    End If
                End If
                file.WriteLine("      <LicenceFileID>No</LicenceFileID>")
                If autogen = True Then
                    file.WriteLine("      <LicenceFile>Yes</LicenceFile>")
                ElseIf licencefile = True Then
                    file.WriteLine("      <LicenceFile>Yes</LicenceFile>")
                    file.WriteLine("      <ActivationKey>Yes</ActivationKey>")
                Else
                    file.WriteLine("      <LicenceFile>No</LicenceFile>")
                    file.WriteLine("      <ActivationKey>Yes</ActivationKey>")
                End If
                file.WriteLine("      <Modules>")
                For i = 0 To UBound(items)
                    file.WriteLine("         <Module>")
                    file.WriteLine("            <ModuleNumber>" & items(i) & "</ModuleNumber>")
                    If autogen = True Then
                        file.WriteLine("            <DemoVersion>Yes</DemoVersion>")
                        file.WriteLine("            <Days>10</Days>")
                        file.WriteLine("            <Value>1</Value>")
                        file.WriteLine("            <AbsoluteDays>Yes</AbsoluteDays>")
                    ElseIf lictype = "extdemo" Then
                        file.WriteLine("            <DemoVersion>Yes</DemoVersion>")
                        file.WriteLine("            <Days>" & licenceCount(i) & "</Days>") 'Licence count field represent days in this case
                        file.WriteLine("            <Value>1</Value>")
                        file.WriteLine("            <AbsoluteDays>No</AbsoluteDays>")
                    Else
                        'When generating a new licence file 0 licences means a demo licence
                        If licencefile = True And licenceCount(i) = 0 Then
                            file.WriteLine("            <DemoVersion>Yes</DemoVersion>")
                            file.WriteLine("            <Days>10</Days>")
                            file.WriteLine("            <Value>1</Value>")
                        Else
                            file.WriteLine("            <DemoVersion>No</DemoVersion>")
                            file.WriteLine("            <Days>0</Days>")
                            file.WriteLine("            <Value>" & licenceCount(i) & "</Value>")
                        End If
                        file.WriteLine("            <AbsoluteDays>Yes</AbsoluteDays>")
                    End If
                    file.WriteLine("         </Module>")
                Next
                file.WriteLine("      </Modules>")
                file.WriteLine("   </Product>")
                file.WriteLine("</Connector>")
                file.Close


                'Order
                Set file = fso.CreateTextFile(TempDir + "\" + OrderFile, True)
                file.WriteLine("<LP-Order>")
                file.WriteLine("   <Product>")
                file.WriteLine("      <ProductID>1111</ProductID>")
                file.WriteLine("   </Product>")
                If Request("copyprotection") <> "false" Then
                    file.WriteLine("   <Registration>")
                    file.WriteLine("      <InstallationCode>" & Request("code") & "</InstallationCode>")
                    file.WriteLine("   </Registration>")
                End If
                file.WriteLine("</LP-Order>")
                file.Close()

                Set file = Nothing
                Set fso = Nothing
                
                'Automatically store all form variable in session so we can obtain it when the licence key is generated
                For Each item In Request.QueryString
                    If Left(item, 12) <> "licencecount" Then
                        Session(item) = Request.QueryString(item)
                    End If
                Next
                'Store licence count separately as this list may be manipulated by the logic
                For i = 0 To itemCount - 1
                    Session("licencecount" & items(i)) = licenceCount(i)
                Next

                If autogen = True Then OutFile = OutFile & "&autogen=true"
                If licencefile = True Then OutFile = OutFile & "&licencefile=on"
                Response.Redirect "generatekey.aspx?outfile=" & OutFile
            Else
                error = "You must choose at least one module."
            End If
        End If
    End If

Case "post-generate"
    keypath = TempDir + "\" + Request("filename")
    Set fso = Server.CreateObject("Scripting.FileSystemObject")

    'Check return code for an error
    If Int(Request("retcode")) <> 0 Then
        Response.Write "Automatic Licence Generator failed with return code " & Request("retcode") & "."
        Response.End
    ElseIf autogen = False And Not fso.FileExists(keypath) Then
        Response.Write "Generated key file does not exist."
        Response.End
    End If

    'Retrieve generated key(s)
    If autogen = False Then
        Call MakeConn(con)

        'Read off xml key-file, replace encoding argument (MSXML does not like 'UTF-8')
        Set file = fso.OpenTextFile(keypath, ForReading)
        xmlData = Replace(file.ReadAll, "encoding='utf-8'", "encoding='iso-8859-1'")
        file.Close
        Set file = Nothing

        'Parse xml
        Set objXML = Server.CreateObject("Microsoft.XMLDOM")
        Set objLst = Server.CreateObject("Microsoft.XMLDOM")
        objXML.async = False
        objXML.LoadXML(xmlData)
        If objXML.parseError.errorCode <> 0 Then
            Response.Write "An error occured while trying to parse generated licence file.<p>"
            Response.Write "Reason: " & objXML.parseError.reason & "<br>"
            Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
            Response.Write "Line: " & objXML.parseError.line & "<br>"
            Response.Write "</p>"
            Response.End
        End If
        
        'Store order data
        SQL = "SELECT TOP 1 * from licenceorder"
        Call MakeRs_add2(rst,con,SQL) 'MakeRs_add2 gives us a resyncable recordset
        rst.AddNew
        rst("date")             = now
        rst("copyprotection")   = Session("copyprotection")
        rst("hwid")             = UCase(Session("code"))
        rst("ip")               = Request.ServerVariables("REMOTE_HOST")
        rst("userid")           = Session("userid")

        'Set customer number for this order if specified, create new customer record if customer number doesn't exist
        If IsNumeric(Session("customernum")) Then
            SQL = "IF NOT EXISTS(SELECT 1 FROM customer WHERE customernum = " & Session("customernum") & ") INSERT INTO customer (customernum, name) VALUES(" & Session("customernum") & ", '')"
            Call con.Execute(SQL)

            rst("customernum") = Session("customernum")
        ElseIf Session("customernum") <> vbNullString Then
            rst("notes") = Left(Session("customernum"), 50) 'Save any notes from customer field
        End If

        'Get order id of newly created record
        rst.Resync 'rst.Update
        orderid = rst("orderid")
        Call Destroy(rst)

        'List modules found in xml
        Set objLst = objXML.getElementsByTagName("ActKey")
        For i = 0 to (objLst.length - 1)
            key = vbNullString: name = vbNullString

            For j = 0 to objLst.item(i).childNodes.Length - 1
                Select Case objLst.item(i).childNodes(j).nodeName
                Case "KeyType"
                    keytype = objLst.item(i).childNodes(j).text
                Case "TheKey"
                    key = objLst.item(i).childNodes(j).text
                Case "Modulename"
                    name = objLst.item(i).childNodes(j).text
                Case "ModuleID"
                    id = objLst.item(i).childNodes(j).text
                End Select
            Next

            'Ignore demo keys used in licence file (activation key should be logged)
            If Not (Session("licencefile") = "on" And Session("licencecount" & id) = 0 And keytype <> "1") Then
                'Store generated key
                SQL = "SELECT TOP 1 * FROM licence"
                Call MakeRs_add(rst,con,SQL)
                rst.AddNew
                rst("orderid") = orderid
                rst("licencekey") = key
                rst("licencecount") = Session("licencecount" & id)
                If keytype = "1" Then
                    'Copy Protection Activation key
                    rst("modulename")   = "Activation key"
                    rst("moduleid")     = "1"
                Else
                    rst("modulename")   = name
                    rst("moduleid")     = id
                End If
                If Session("type") = "extdemo" Then
                    rst("demo") = True
                Else
                    rst("demo") = False
                End If
                rst.Update
                Call Destroy(rst)
            End If
        Next
        Call Destroy(con)
        
        Set objLst = Nothing
        Set objXML = Nothing
    End If

    'Delete generated files (keypath does not exist when a demo licence file is generated)
    On Error Resume Next
    fso.DeleteFile(keypath)
    fso.DeleteFile(TempDir + "\" + OrderFile)
    fso.DeleteFile(TempDir + "\" + ProductFile)
    On Error Goto 0

    'Download licence file if autogeneration (if licencefile was checked manually download will be done after redirect)
    If autogen = True Then
        DownloadFile(TempDir + "\" + "HaglofManagementSystems.lic")
        fso.DeleteFile(TempDir + "\" + "HaglofManagementSystems.lic")
    End If

    Set fso = Nothing

    If autogen = False Then
        'Clear all session variables, keep log-in values
        admin = Session("admin")
        retailerid = Session("retailerid")
        userid = Session("userid")
        supervisor = Session("supervisor")

        Session.Contents.RemoveAll()

        Session("admin") = admin
        Session("retailerid") = retailerid
        Session("userid") = userid
        Session("supervisor") = supervisor

        Response.Redirect "licence-view.asp?cmd=viewlast&id=" & orderid & "&licencefile=" & licencefile
    Else
        Response.Status = "200 OK"
    End If
End Select

If error <> vbNullString Then
    %><div align="center"><font color="red"><%=error %></font></div><hr /><%
End if

If cmd = vbNullString Or error <> vbNullString Then
    %>

    <form name="f" method="get" action="newlicence.asp">
    <input type="hidden" name="cmd" value="generate" />
    <table border="0">
    <tr>
    <td></td>
    <td style="width:80px"><b>Id</b></td>
    <td style="width:150px"><b>Module</b></td>
    <td style="width:180px"><b>Number of new<br />licences/days</b></td>
    <td style="width:150px"><b>Tag</b></td>
    </tr>

    <%
    'Parse xml
    Set objXML = Server.CreateObject("Microsoft.XMLDOM")
    Set objLst = Server.CreateObject("Microsoft.XMLDOM")
    objXML.async = False
    objXML.Load(ProjectDir & "\" & ProjectFile)
    If objXML.parseError.errorCode <> 0 Then
        Response.Write "An error occured while trying to parse licence project file.<p>"
        Response.Write "Reason: " & objXML.parseError.reason & "<br>"
	    Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
	    Response.Write "Line: " & objXML.parseError.line & "<br>"
	    Response.Write "</p>"
        Response.End
    End If

    Set objLst = objXML.getElementsByTagName("Module")

    'List modules found in xml
    ct = 0
    For i = 0 to (objLst.length - 1)
        id = vbNullString: name = vbNullString: tag = vbNullString

        For j = 0 to objLst.item(i).childNodes.Length - 1
            Select Case objLst.item(i).childNodes(j).nodeName
            Case "ModuleName"
                name = objLst.item(i).childNodes(j).text
            Case "ModuleNumber", "ModuleID"
                id = objLst.item(i).childNodes(j).text
            Case "TagValueModule"
                tag = objLst.item(i).childNodes(j).text
            End Select
        Next

        'List only those modules which users has permissions for
        SQL = "SELECT * FROM retailerpermission WHERE retailerid = " & Session("retailerid") & " AND moduleid = '" & id & "'"
        Call MakeConn(con)
        Call MakeRs_view(rst,con,SQL)
        If Not rst.Eof Then
            %>
            <tr>
            <td><input type="checkbox" name="module<%=ct %>" <% If Request("module" & ct) = "on" Then %>checked="checked"<%End If %> /></td>
            <td><%=id %><input type="hidden" name="id<%=ct %>" value="<%=id %>" /></td>
            <td><%=name %></td>
            <td><input type="text" name="licencecount<%=id %>" value="<%If Request("licencecount" & id) <> vbNullString Then Response.Write Request("licencecount" & id): Else Response.Write "0" %>" /></td>
            <td><%=tag %></td>
            </tr>
            <%
        End If
        Call Destroy(rst)
        Call Destroy(con)
        
        ct = ct + 1 'Increase counter
    Next
    %>

    </table>

    <p></p>

    <fieldset style="width:220px">
     <legend>Licensing (number of new...)</legend>
     <table>
     <tr><td><input type="radio" name="type" value="newlic" <%If Request("type") = "newlic" Or Request("type") = vbNullString Then %>checked="checked"<%End If %> />&nbsp;New licence (licences)</td></tr>
     <tr><td><input type="radio" name="type" value="extdemo" <%If Request("type") = "extdemo" Then %>checked="checked"<%End If %> />&nbsp;Extend demo (days)</td></tr>
     </table>
    </fieldset>

    <p></p>

    <table>
    <tr>
    <td>Copy protection</td>
    <td><select name="copyprotection">
      <option value="1" <% If Request("copyprotection") = "1" Then Response.Write "selected=""selected""" %>>1 - Volume ID</option>
      <option value="2" <% If Request("copyprotection") = "2" Then Response.Write "selected=""selected""" %>>2 - MAC address</option>
      <option value="3" <% If Request("copyprotection") = "3" Then Response.Write "selected=""selected""" %>>3 - Hostname</option>
      <option value="4" <% If Request("copyprotection") = "4" Then Response.Write "selected=""selected""" %>>4 - Netbios name (local)</option>
      <option value="5" <% If Request("copyprotection") = "5" Then Response.Write "selected=""selected""" %>>5 - Volume ID + path</option>
      <option value="6" <% If Request("copyprotection") = "6" Then Response.Write "selected=""selected""" %>>6 - MAC address + path</option>
      <option value="7" <% If Request("copyprotection") = "7" Then Response.Write "selected=""selected""" %>>7 - IP address</option>
      <option value="8" <% If Request("copyprotection") = "8" Then Response.Write "selected=""selected""" %>>8 - Code 1,2,3 + path</option>
      <option value="9" <% If Request("copyprotection") = "9" Then Response.Write "selected=""selected""" %>>9 - Code 1,2,3</option>
      <option value="10" <% If Request("copyprotection") = "10" Then Response.Write "selected=""selected""" %>>10 - Hostname + path</option>
      <option value="11" <% If Request("copyprotection") = "11" Or Request("copyprotection") = vbNullString Then Response.Write "selected=""selected""" %>>11 - USB memory</option>
      <option value="12" <% If Request("copyprotection") = "12" Then Response.Write "selected=""selected""" %>>12 - Windows domain</option>
    </select></td>
    <tr><td colspan="2"><div style="color:red;font-size:x-small">Note! Use a code including path for server licences</div></td></tr>
    </tr>
    <tr><td>Installation code</td><td><input type="text" name="code" maxlength="8" value="<%=Request("code") %>" /></td></tr>
    <tr><td><input type="checkbox" name="activationkey" checked="checked" />Generate activation key</td></tr>
    <tr><td><input type="checkbox" name="licencefile" <%If Request("licencefile") = "on" Then %>checked="checked"<%End If %> onclick="licfile()" />Generate licence file</td></tr>
    <tr style="height:40px" valign="bottom"><td>Customerno.</td><td><input type="text" name="customernum" maxlength="50" value="<%=Request("customernum") %>" /></td></tr>
    <tr><td colspan="2"><a href="#" onclick="window.open('customer-search.asp', 'customerreg', 'height=600,width=450,resizable=0,scrollbars=1')"><font size="2">Search customer register</font></a></td></tr>
    </table>

    <p><input type="submit" value="Create licences" /></p>
    </form>

    <%
End If

Set objLst = Nothing
Set objXML = Nothing
%>

<%
Sub CheckPermission(module, con)
    Dim rst, SQL

    'Check if retailer has permission for specified module
    SQL = "SELECT * FROM retailerpermission WHERE retailerid = " & Session("retailerid") & " AND moduleid = '" & FilterInput(module) & "'"
    Call MakeRs_view(rst,con,SQL)
    If rst.Eof Then
        If error = vbNullString Then
            error = "You do not have permission to generate licences for the following module(s): "
        Else
            error = error & ", "
        End If
        error = error & module
    End If
    Call Destroy(rst)
End Sub
%>

<p><a href="./">Back to main menu</a></p>
 
</body>
</html>
