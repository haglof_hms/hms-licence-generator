USE [hms_licence]
GO

/****** Object:  Table [dbo].[licenceorder]    Script Date: 02/11/2009 09:19:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[licenceorder](
	[orderid] [int] IDENTITY(1,1) NOT NULL,
	[date] [smalldatetime] NOT NULL,
	[copyprotection] [tinyint] NOT NULL,
	[hwid] [char](8) NOT NULL,
	[ip] [varchar](20) NOT NULL,
	[userid] [int] NULL,
	[customernum] [int] NULL,
	[notes] [varchar](50) NULL,
 CONSTRAINT [PK_licenceorder] PRIMARY KEY CLUSTERED 
(
	[orderid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[licenceorder]  WITH CHECK ADD  CONSTRAINT [FK_licenceorder_useraccount] FOREIGN KEY([userid])
REFERENCES [dbo].[useraccount] ([userid])
ON UPDATE CASCADE
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[licenceorder] CHECK CONSTRAINT [FK_licenceorder_useraccount]
GO

