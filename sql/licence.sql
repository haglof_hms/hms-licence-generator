USE [hms_licence]
GO

/****** Object:  Table [dbo].[licence]    Script Date: 02/03/2009 13:19:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[licence](
	[orderid] [int] NOT NULL,
	[modulename] [varchar](200) NOT NULL,
	[moduleid] [varchar](50) NOT NULL,
	[licencekey] [char](40) NOT NULL,
	[licencecount] [smallint] NULL,
	[demo] [bit] NULL,
 CONSTRAINT [PK_licence] PRIMARY KEY CLUSTERED 
(
	[orderid] ASC,
	[moduleid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[licence]  WITH CHECK ADD  CONSTRAINT [FK_licence_licenceorder] FOREIGN KEY([orderid])
REFERENCES [dbo].[licenceorder] ([orderid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[licence] CHECK CONSTRAINT [FK_licence_licenceorder]
GO

