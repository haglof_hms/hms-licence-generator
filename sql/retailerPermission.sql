USE [hms_licence]
GO

/****** Object:  Table [dbo].[retailerPermission]    Script Date: 01/23/2009 13:09:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[retailerPermission](
	[retailerId] [int] NOT NULL,
	[moduleId] [varchar](10) NOT NULL,
 CONSTRAINT [PK_retailerPermission] PRIMARY KEY CLUSTERED 
(
	[retailerId] ASC,
	[moduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[retailerPermission]  WITH CHECK ADD  CONSTRAINT [FK_retailerPermission_retailer] FOREIGN KEY([retailerId])
REFERENCES [dbo].[retailer] ([entityid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[retailerPermission] CHECK CONSTRAINT [FK_retailerPermission_retailer]
GO

