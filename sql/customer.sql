USE [hms_licence]
GO

/****** Object:  Table [dbo].[customer]    Script Date: 02/17/2009 08:58:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customer](
	[customernum] [int] NOT NULL,
	[company] [nvarchar](50) NULL,
	[name] [nvarchar](50) NOT NULL,
	[zip] [varchar](10) NULL,
	[city] [nvarchar](50) NULL,
	[country] [nvarchar](50) NULL,
	[phone] [varchar](50) NULL,
	[fax] [varchar](50) NULL,
 CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED 
(
	[customernum] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

