USE [hms_licence]
GO

/****** Object:  Table [dbo].[useraccount]    Script Date: 01/28/2009 10:42:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[useraccount](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [char](64) NULL,
	[supervisor] [bit] NOT NULL,
	[retailerId] [int] NOT NULL,
 CONSTRAINT [PK_useraccount] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[useraccount]  WITH CHECK ADD  CONSTRAINT [FK_useraccount_retailer] FOREIGN KEY([retailerId])
REFERENCES [dbo].[retailer] ([entityid])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[useraccount] CHECK CONSTRAINT [FK_useraccount_retailer]
GO

