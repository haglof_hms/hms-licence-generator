<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Int(Session("retailerid")) <= 0 Then Response.Redirect "./" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title></head>
<body <%If Request("cmd") = vbNullString Then %>onload="document.getElementById('customernum').focus()"<%End If %>>

<%
Dim con, rst, SQL, error
Dim conditions, customerid, company, contactname, city, zip, country, phone, fax

Call MakeConn(con)

Select Case Request("cmd")
Case "addcustomer", "update"
    customerid  = Trim(FilterInput(Request("customernum")))
    company     = Trim(FilterInput(Request("company")))
    contactname = Trim(FilterInput(Request("contactname")))
    city        = Trim(FilterInput(Request("city")))
    zip         = Trim(FilterInput(Request("zip")))
    country     = Trim(FilterInput(Request("country")))
    phone       = Trim(FilterInput(Request("phone")))
    fax         = Trim(FilterInput(Request("fax")))

    If Not IsNumeric(customerid) Or (company = vbNullString And contactname = vbNullString) Then
        Call Destroy(con)
        Response.Redirect "customer-register.asp"
    End If

    If Request("cmd") = "update" Then
        'Update existing record
        SQL = "UPDATE customer SET company = '" & company & "', name = '" & contactname & "', city = '" & city & "', zip = '" & zip & "', country = '" & country & "', phone = '" & phone & "', fax = '" & fax & "' WHERE customernum = " & customerid & " AND name = '" & contactname & "'"
        Call con.Execute(SQL)
    Else
        'Add new record
        SQL = "SELECT customernum FROM customer WHERE customernum = " & customerid & " AND name LIKE '" & contactname & "'"
        Call MakeRs_view(rst,con,SQL)
        If Not rst.Eof Then error = "Customer already exist."
        Call Destroy(rst)

        If error = vbNullString Then
            SQL = "INSERT INTO customer (customernum, company, name) VALUES('" & customerid & "', '" & company & "', '" & contactname & "')"
            Call con.Execute(SQL)
        End If
    End If

    If error <> vbNullString Then
        %><div style="color:red"><%=error %></div><%
    Else
        Response.Write "Done."
    End If
    %><hr /><%


Case "viewedit", "addcopy"
    SQL = "SELECT * FROM customer WHERE customernum = " & FilterInput(Request("customerid")) & " AND name LIKE '" & FilterInput(Request("name")) & "'"
    Call MakeRs_view(rst,con,SQL)
    customerid  = rst("customernum")
    company     = rst("company")
    contactname = rst("name")
    city        = rst("city")
    zip         = rst("zip")
    country     = rst("country")
    phone       = rst("phone")
    fax         = rst("fax")
    Call Destroy(rst)
End Select

Call Destroy(con)
%>

<form id="f1" action="customer-register.asp" method="post">
<%If Request("cmd") = "viewedit" Then %>
    <input type="hidden" name="cmd" value="update" />
    <input type="hidden" name="customernum" value="<%=customerid %>" />
<%Else %>
    <input type="hidden" name="cmd" value="addcustomer" />
<%End If %>
<table>
<tr><td>Customerno.*</td><td><input type="text" style="width:300px" name="customernum" value="<%=customerid %>" <%If Request("cmd") = "viewedit" Then %>disabled<%End If %> maxlength="10" /></td></tr>
<tr><td>Company*</td><td><input type="text" style="width:300px" name="company" value="<%=company %>" maxlength="50" /></td></tr>
<tr><td>Contact name*</td><td><input type="text" style="width:300px" name="contactname" value="<%=contactname %>" maxlength="50" /></td></tr>
<tr><td>City</td><td><input type="text" style="width:200px" name="city" value="<%=city %>" maxlength="50" /></td></tr>
<tr><td>Zip</td><td><input type="text" style="width:200px" name="zip" value="<%=zip %>" maxlength="50" /></td></tr>
<tr><td>Country</td><td><input type="text" style="width:200px" name="country" value="<%=country %>" maxlength="50" /></td></tr>
<tr><td>Phone</td><td><input type="text" style="width:200px" name="phone" value="<%=phone %>" maxlength="50" /></td></tr>
<tr><td>Fax</td><td><input type="text" style="width:200px" name="fax" value="<%=fax %>" maxlength="50" /></td></tr>
<%If Request("cmd") = "viewedit" Then %>
    <tr><td colspan="2"><input type="submit" value="Update" /></td></tr>
<%Else %>
    <tr><td colspan="2"><input type="submit" value="Add customer" /></td></tr>
<%End If %>
</table>
</form>

<%If Request("cmd") <> "viewedit" Then %>
    <p><a href="#" onclick="window.open('customer-search.asp?opt=addremove', 'customerreg', 'height=600,width=550,resizable=0,scrollbars=1')"><font size="2">Search customer register / Remove customer</font></a></p>
<%End If %>


<p><a href="./">Back to main menu</a></p>

</body>
</html>
