<% Option Explicit %>
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Session("admin") <> "true" Then Response.Redirect "admin.asp" %>

<html>
<body>

<%
Dim fso, file
Dim objXML, objLst
Dim i, j, item
Dim id, name, value, days, demo, tag


If Request("cmd") = "update" Then
    Set fso = Server.CreateObject("Scripting.FileSystemObject")
    Set file = fso.CreateTextFile(ProjectDir & "\" & ProjectFile, True)

    file.WriteLine("<?xml version='1.0' encoding='iso-8859-1'?>")
    file.WriteLine("<LicenceProtector>")
    file.WriteLine("	<Default>")
    %>
        <!-- The name of the project -->
    <%
    file.WriteLine("		<ProjectName>HaglofManagementSystems</ProjectName>")
    %>
        <!-- ####################################################### -->
        <!-- SecurityLevel: Basic or advanced -->
        <!-- ADVANCED uses ProjectSecureKey and ReadKey and WriteKey and therefore is more secure than -->
        <!-- BASIC that only uses one key as ProjectSecureKey -->
    <%
    file.WriteLine("		<SecurityLevel>basic</SecurityLevel>")
    %>
		<!-- ProjectSecureKey or PSK is necessary in SIMPLE and ADVANCED SecurityLevel -->
    <%
    file.WriteLine("		<ProjectSecureKey>1234567890</ProjectSecureKey>")
    %>
		<!-- ReadKey and WriteKey are only necessary in ADVANCED SecurityLevel and not used in SIMPLE -->
    <%
    file.WriteLine("		<ReadKey>r1oKa9b1xGQN81oMqAQ1dfrEz28fgqg0SP8as1Vyu5W24xEjB1k8dC81Cs8wI1hW5WJ1NvYen1S6jcJ1wJJ9h23t6th1MDhiM0U5ymu0Xkx301OTsnI1fx4Hh1nApEN1A5CYQ0SNNR42akbT91xCbib23wVHF2auDKu1gzUDN1iYZ4E0WpDrV2bpySF1BhQ8925VrHo1R9JQo1CqOJY1mwXu91emrA30SPbuF24C0Hc1DxQOj0NbSMy0WrhGZ1iPT8j1ei4h91r5U8w206mhX1cfAYk2ahH3J</ReadKey>")
    file.WriteLine("		<WriteKey>r26YjnF2dMrIG1t2ERJ1dp4mD1XHSWX1SjNIK1tdpPB0YFSkb29i6iR1mlFBp0RFUu61R0pYr1vmd4w29n6TR1xCqSj1ez8M91pNUP92acgSo1kdayT1tkyb729lFJV0NkXkp1qU6LP0WqWfd29mh6p1dmPzu17DD27</WriteKey>")
    %>
		<!-- If you want to use internal security you can use ReadKeyProtected and WriteKeyProtected -->
		<!-- These are the encrypted versions ot ReadKey and WriteKey and can be decrypted using the Project Master Key -->
		<!-- So whithout knowing the Project Master Key this Project file can't be use at all. -->
		<!-- Notice: If you want to use thi extended security mechanism than remove the unprotected ReadKey and WriteKey (above) from this file. -->
    <%
    file.WriteLine("		<ReadKeyProtected>r1caneZ1VwRxC0Xw4if1c2FNC0U8NOP0SOS6l1yL8QC1Q0NXy1Cm92v24JZ3H1xPEdh2aqwO91pVQZ31cbIGg1gSn7D2dAQ4h1gHXjP1DpAcV1fGran0YCQti2dTilp1VBMmu0N4vVw134Ebi11WOyE0Xx9Td0WnFM40Wiqbd23EllJ1dn7Wn12XZxU2cApiE0ZH6fi0Vfkbp1DvzX913aHQL1A9r4l1NCPsC1B1VSW1uddrP1diJej1QXWGe1dpBzX1Cs9sr1mxO5j218JNf1vDcDd1pTGAR1ChrKt2dI10S2f17Xt1j6aCX10W7h40NcbT91Cakp91OCbq11iYKmo2atAXq1CnbqP126pGo0XwVIe20fqDk2dDmvL1TsY3P1MzGgl1xHmDb1wIlTj1Scl501EGZKw0RzB9Y1PN4Es282Rdy1nwiZ71MsyZx1xPEel10FAJD29kmu2131ioi0SKt3S1CrjiV1UsTJ91B7Ewq1CnbqQ1mt5Fu2f1Ikr1qVK4h1zTwiu2bmj9j1PUbZz1WJHqr2bBXRJ205MBp0YGnTJ26Toe31vDePE25LLNr2ama9Z1Nz1zA17JFsp</ReadKeyProtected>")
    file.WriteLine("		<WriteKeyProtected>r1EDuzs1YVg1f11YoUQ24wfpz1vBT3I1jYkAM29aZ131Msdy50WhlUT1OTbot1oJ6h124BZ6S1vBT3F1hWBqM1xJ22H1OQd7A1Bej0v1VKVqz0U4nLr1UyW9f0WqKc51TaADS1DnUsJ29ewZ51EyOJU0RBPGQ1UtaLK0WnVnn1Q2YXz1j68xX2ajnyo0ZxtC31SbxVR1NsG6S0YzeLI2bzMGM1scHv2273vj20N9L2H1AbDVZ1Bee661dtqK01BevJm1lkuLc1z15GJ1Eqz0o1PToTB1vrsgJ1BegtF133yV6121bRT1ErlYh1wJWS221r70A1nvJql0SSXR7</WriteKeyProtected>")
    %>
		<!-- ####################################################### -->
		<!-- default name for a generated Licence File -->
    <%
    file.WriteLine("		<NameLicenceFile>HaglofManagementSystems</NameLicenceFile>")
    %>
		<!-- the ID of the next generated Licence file will be ...-->
    <%
    file.WriteLine("		<NextLicFileKey>133</NextLicFileKey>")
    %>
		<!-- you can use the tag value as you like. Take a look in the documentation for further information.-->
    <%
    file.WriteLine("		<TagValue>This is my tag</TagValue>")
    %>
		<!-- Copy Protection mechanism, 0 means off, for the other values please take a look in the documentation -->
    <%
    file.WriteLine("		<CopyProtection>0</CopyProtection>")
    %>
		<!-- you can use more than one manufacturers, simply use a semicolon between them -->
    <%
    file.WriteLine("		<Manufacturers>Hagl�f Sweden AB</Manufacturers>")
    %>
		<!-- the default customer name -->
    <%
    file.WriteLine("		<CustomerName>Trial Version</CustomerName>")
    %>
		<!-- ####################################################### -->
		<!-- the URL of your Licence Protector Web Activation Service -->
    <%
    file.WriteLine("		<WebServiceURL>http://oden/lpweb/lpws.asmx</WebServiceURL>")
    %>
		<!-- if you do not want the embedded form then you can turn it off -->
    <%
    file.WriteLine("		<ShowWAStartPage>yes</ShowWAStartPage>")
    file.WriteLine("		<ShowWAProgressPage>yes</ShowWAProgressPage>")
    file.WriteLine("		<ShowWAResultPage>yes</ShowWAResultPage>")
    %>
		<!-- ####################################################### -->
		<!-- Tamper detection settings are used to control the Run Number behaviour. Please check the documentation. -->
    <%
    file.WriteLine("		<TamperDetection>off</TamperDetection>")
    %>
		<!-- Possible values for the extended Tamper Detection Mode are -->
		<!-- off    = no Tamper Detection with Run Number -->
		<!-- auto   = Tamper Detection with Run Number, automatic first entry -->
		<!-- manual = Tamper Detection with Run Number, first entry must be created by the app -->
    <%
    file.WriteLine("		<TamperDetectionMode>off</TamperDetectionMode>")
    %>
		<!-- TamperDetectionGraceTimes determines the size of the negative difference of the Run Number -->
    <%
    file.WriteLine("		<TamperDetectionGraceTimes>0</TamperDetectionGraceTimes>")
    %>
		<!-- ####################################################### -->
		<!-- Settings for the CheckLicence feature. Valid values are -->
		<!-- off         = CheckLicence is off -->
		<!-- on          = CheckLicence is on, regularly checks on the Web Activation Service are necessary -->
		<!-- frozen 	 = the last validation on the Web Activation Service failed and so the licence is frozen until a sucessful CheckLicence -->
		<!-- deactivated = CheckLicence is turned off -->
    <%
    file.WriteLine("		<LicenceVerification>on</LicenceVerification>")
    %>
		<!-- CheckLicence is only executed until that date is reached -->
    <%
    file.WriteLine("		<EndVerification>")
    file.WriteLine("		</EndVerification>")
    file.WriteLine("		<AllowSetVal>yes</AllowSetVal>")
    file.WriteLine("	</Default>")

    'Write modules to file
    file.WriteLine("    <LicenceModules>")
    i = 0
    Do
        'Loop Until we no more modules are found
        If Request("module" & i) = vbNullString Then Exit Do

        'Check if this module is checked
        If Request("chk" & i) = "on" Then
            file.WriteLine("        <Module>")
            file.WriteLine("            <LocalReuse>yes</LocalReuse>")
            file.WriteLine("            <ModuleID>" & Request("id" & i) & "</ModuleID>")
            file.WriteLine("            <ModuleName>" & Request("name" & i) & "</ModuleName>")
            file.WriteLine("            <ModuleType>R</ModuleType>")
            file.WriteLine("            <Value>" & Request("value" & i) & "</Value>")
            file.WriteLine("            <AbsoluteLics>no</AbsoluteLics>")
            file.WriteLine("            <Days>" & Request("days" & i) & "</Days>")
            file.WriteLine("            <AbsoluteDays>yes</AbsoluteDays>")
            file.WriteLine("            <ValidUntilDay></ValidUntilDay>")
            file.WriteLine("            <TagValueModule>" & Request("tag" & i) & "</TagValueModule>")
            file.WriteLine("            <Demoversion>" & Request("demo" & i) & "</Demoversion>")
            file.WriteLine("            <LoadAsDefault>Yes</LoadAsDefault>")
            file.WriteLine("            <AllowDeactivate>yes</AllowDeactivate>")
            file.WriteLine("            <WebActivation>none</WebActivation>")
            file.WriteLine("        </Module>")
        End If

        i = i + 1
    Loop
    file.WriteLine("    </LicenceModules>")
    file.WriteLine("</LicenceProtector>")
    

    file.Close
    Set file = Nothing
    Set fso = Nothing
    
    Response.Redirect "edit-project.asp"
End If


If Request("cmd") = vbNullString Then
    'Parse project file xml
    Set objXML = Server.CreateObject("Microsoft.XMLDOM")
    Set objLst = Server.CreateObject("Microsoft.XMLDOM")
    objXML.async = False
    objXML.Load(ProjectDir & "\" & ProjectFile)
    If objXML.parseError.errorCode <> 0 Then
        Response.Write "An error occured while trying to parse licence project file.<p>"
        Response.Write "Reason: " & objXML.parseError.reason & "<br>"
        Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
        Response.Write "Line: " & objXML.parseError.line & "<br>"
        Response.End
    End If

    %>
    <form method=post action="edit-project.asp">
    <input type=hidden name="cmd" value="update">
    <table border=0>
    <tr>
    <td></td>
    <td width="80px"><b>Id</b></td>
    <td width="190px"><b>Module</b></td>
    <td width="50px"><b>Lic.ct.</b></td>
    <td width="50px"><b>Days</b></td>
    <td width="60px"><b>Demo</b></td>
    <td width="160px"><b>Tag</b></td>
    </tr>
    <%

    Set objLst = objXML.getElementsByTagName("Module")

    'Add all modules to query string
    For i = 0 To objLst.length
        id = vbNullString
        name = vbNullString
        value = vbNullString
        days = vbNullString
        demo = vbNullString
        tag = vbNullString

        If i < objLst.length Then
            For j = 0 To objLst.item(i).childNodes.Length - 1
                Select Case objLst.item(i).childNodes(j).nodeName
                Case "ModuleNumber", "ModuleID"
                    id = objLst.item(i).childNodes(j).text
                Case "ModuleName"
                    name = objLst.item(i).childNodes(j).text
                Case "Value"
                    value = objLst.item(i).childNodes(j).text
                Case "Days"
                    days = objLst.item(i).childNodes(j).text
                Case "Demoversion"
                    demo = objLst.item(i).childNodes(j).text
                Case "TagValueModule"
                    tag = objLst.item(i).childNodes(j).text
                End Select
            Next
        End If

        %>
        <tr>
        <input type=hidden name="module<%=i %>" value="on">
        <td><input type=checkbox name="chk<%=i %>" <%If id <> vbNullString Then Response.Write "checked"%>></td>
        <td><input type=text name="id<%=i %>" value="<%=id %>" style="width:70px"></td>
        <td><input type=text name="name<%=i %>" value="<%=name %>" style="width:180px"></td>
        <td><input type=text name="value<%=i %>" value="<%=value %>" style="width:40px"></td>
        <td><input type=text name="days<%=i %>" value="<%=days %>" style="width:40px"></td>
        <td>
        <select name="demo<%=i %>">
        <option value="yes" <%If LCase(demo) = "yes" Then Response.Write "selected"%>>Yes</option>
        <option value="no" <%If LCase(demo) = "no" Then Response.Write "selected"%>>No</option>
        </select>
        </td>
        <td><input type=text name="tag<%=i %>" value="<%=tag %>" style="width:150px"</td>
        </tr>
        <%
    Next

    Set objLst = Nothing
    Set objXML = Nothing

    %>
    <tr><td colspan="7"><font size="2">Uncheck a module to remove it.</font></td></tr>
    <tr><td colspan="7"><font size="2" color="red">Note! Make sure to add appropriate retailer permissions after adding a new module.</font></td></tr>
    <tr style="height:40px;" valign=bottom><td colspan=2><input type=submit value="Update"></td></tr>
    </table>
    </form>
    <%
End If
%>

<p><a href="admin.asp">Back to main menu</a>

</body>
</html>
