<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Int(Session("retailerid")) <= 0 And Session("supervisor") = "true" Then Response.Redirect "./" %>
<% Server.ScriptTimeout = 120 %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>

<%
Dim i, cmd, item, orderid, error
Dim con, rst, rst2, SQL

Server.ScriptTimeout = 120 '2 minutes

cmd = Request("cmd")

Select Case cmd
Case "generate"
    'Check constraints
    If Not IsNumeric(Request("pid")) Or Request("pid") = vbNullString Or _
       Not IsNumeric(Request("serial")) Or Request("serial") = vbNullString Or _
       Not IsNumeric(Request("level")) Or Request("level") = vbNullString Then
        error = "You must enter product id, serial and level."
    End If

    If error = vbNullString Then
        'Automatically store all form variable in session so we can obtain it when the licence key is generated
        For Each item In Request.QueryString
            Session(item) = Request.QueryString(item)
        Next

        Response.Redirect "calgen.aspx?pid=" & Request("pid") & "&serial=" & Request("serial") & "&level=" & Request("level")
    End If

Case "post-generate"
    'Check return code for an error
    If Int(Request("retcode")) = 0 Then
        Response.Write "Licence generator failed."
        Response.End
    End If

    'Retrieve generated key
    Call MakeConn(con)

    'Store order data
    SQL = "SELECT TOP 1 * from licenceorder"
    Call MakeRs_add2(rst,con,SQL) 'MakeRs_add2 gives us a resyncable recordset
    rst.AddNew
    rst("date")             = now
    rst("copyprotection")   = 0
    rst("hwid")             = Session("serial")
    rst("userid")           = Session("userid")
    rst("ip")               = Request.ServerVariables("REMOTE_HOST")

    'Set customer number for this order if and only if a customer with this number exists
    If IsNumeric(Session("customernum")) Then
        SQL = "SELECT customernum FROM customer WHERE customernum = " & FilterInput(Session("customernum"))
        Call MakeRs_add(rst2,con,SQL)
        If Not rst2.Eof Then
            rst("customernum") = Session("customernum")
        End If
        Call Destroy(rst2)
    End If

    'Get order id of newly created record
    rst.Resync 'rst.Update
    orderid = rst("orderid")
    Call Destroy(rst)
     
    'Store generated key
    SQL = "SELECT TOP 1 * FROM licence"
    Call MakeRs_add(rst,con,SQL)
    rst.AddNew
    rst("orderid")      = orderid
    rst("modulename")   = "Caliper licence"
    rst("moduleid")     = Session("pid")
    rst("licencekey")   = Request("retcode")
    rst.Update
    Call Destroy(rst)
    Call Destroy(con)

    Response.Redirect "licence-view.asp?cmd=viewlast&id=" & orderid
End Select

If error <> vbNullString Then
    %><div align="center"><font color="red"><%=error %></font></div><hr /><%
End if

If cmd = vbNullString Or error <> vbNullString Then
    %>

    <form id="form1" method="get" action="caliperlic.asp">
    <input type="hidden" name="cmd" value="generate" />
    <table border="0">
    <tr><td style="width:90px">Serial</td><td><input type="text" name="serial" style="width:140px" value="<%=Request("serial") %>" /></td></tr>
    <tr><td>Product Id</td><td><input type="text" name="pid" style="width:140px" value="<%=Request("pid") %>" /></td></tr>
    <tr><td>Level</td><td>
    <select name="level" style="width:140px">
    <%For i = 1 To 10 %>
        <option><%=i %></option>
    <%Next %>
    </select>
    </td></tr>
    <tr style="height:40px" valign="bottom"><td>Customerno.</td><td><input type="text" name="customernum" value="<%=Request("customernum") %>" /></td></tr>
    <tr><td colspan="2"><a href="#" onclick="window.open('customer-search.asp', 'customerreg', 'height=600,width=450,resizable=0,scrollbars=1')"><font size="2">Search customer register</font></a></td></tr>
    </table>

    <p><input type="submit" value="Create licence" /></p>
    </form>

    <%
End If
%>

<p><a href="./">Back to main menu</a>
 
</body>
</html>
