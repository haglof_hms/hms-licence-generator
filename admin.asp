<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->

<%
Dim con, error

If Request("cmd") = "login" Then
    If CheckADLogin(Request.Form("user"), Request.Form("pwd"), "Programmerare", error) = True Then
        'Get user details from db
        Call MakeConn(con)
        Call DoADLogin(FilterInput(Request.Form("user")), con)
        Call Destroy(con)

        Session("admin") = "true"
    End If
End If
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<body <%If Session("admin") <> "true" Then %>onload="document.getElementById('user').focus()"<%End If %>>
<%If Session("admin") = "true" Then %>
    <a href="edit-project.asp">Edit project file</a><br />
    <a href="newlicence.asp?cmd=autogen&copyprotection=false">Download demo licence file</a><br />
    <p>
    <a href="retailers.asp">Manage retailers</a><br />
    <a href="useraccounts.asp">Manage useraccounts</a><br />
    </p>
    <p>
    <a href="licence-view.asp">Remove licences</a><br />
    </p>
    
    <p><a href="default.asp?cmd=logout">Log out</a></p>
<%Else %>
    <div align="center"><%If error <> vbNullString Then%><font color="red"><%=error %></font><hr /><%End If%>
    <h2>Admin Section</h2>
    <form id="f" action="admin.asp" method="post">
    <input type="hidden" name="cmd" value="login" />
      <table border="0">
        <tr>
          <td><b>Username</b></td>
          <td>
            <input type="text" name="user" value="<%=Request("user") %>" />
          </td>
        </tr>
        <tr>
          <td><b>Password</b></td>
          <td>
            <input type="password" name="pwd" />
          </td>
        </tr>
        <tr>
          <td align="right" colspan="22">
            <input type="submit" value="Login" name="submit" />
          </td>
        </tr>
      </table>
    </form>
    </div>
<%End If %>
</body>
</html>
