using System;

public partial class _Default : System.Web.UI.Page
{
    protected int exec(String prg, String disksn)
    {
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.EnableRaisingEvents = true;
        proc.StartInfo.WorkingDirectory = "C:\\Inetpub\\AdminScripts";
        proc.StartInfo.FileName = "calgen.exe";
        proc.StartInfo.Arguments = String.Concat(String.Concat("es_regcode -prg ", prg), " -disksn ", disksn);
        proc.Start();
        proc.WaitForExit();
        return proc.ExitCode;
    }
}
