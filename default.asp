<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->

<%
Dim con, rst, SQL
Dim error

Call MakeConn(con)

Select Case Request("cmd")
Case "login"
    If CheckADLogin(Request.Form("username"), Request.Form("password"), "Supportgrupp", error) = True Then
        'Get user details from db
        Call DoADLogin(FilterInput(Request.Form("username")), con)
    Else
        'Fallback on typical user accounts
        SQL = "SELECT * FROM useraccount WHERE username LIKE '" & FilterInput(Request.Form("username")) & "'"
        Call MakeRs_view(rst,con,SQL)
        If Not rst.Eof Then
            If rst("password") = pwdencrypt(Request.Form("password")) Then
                'Grant login
                Session("retailerid") = rst("retailerid")
                Session("userid") = rst("userid")
                If rst("supervisor") = True Then Session("supervisor") = "true"
            End If
        End If
        Call Destroy(rst)
    End If

    If Session("retailerid") = vbNullString Then Response.Write "<div align=center><font color=""red"">Incorrect username or password.</font></div><hr>"

Case "logout"
    Session.Abandon
    Response.Redirect "default.asp"
End Select
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<body <%If Session("retailerid") = vbNullString Then %>onload="document.getElementById('username').focus()"<%End If %>>

<% If Int(Session("retailerid")) > 0 Then %>
    <a href="newlicence.asp">HMS licence generator</a><br />
    <a href="caliperlic.asp">Caliper licence generator</a><br />
    <a href="handheldlic.asp">Handheld licence generator</a><br />
    <p>
    <a href="licence-view.asp">View generated licences</a><br />
    <a href="licence-view.asp?cmd=search">Search licences</a><br />
    </p>
    
    <p><a href="customer-register.asp">Customer register</a></p>

    <p><a href="default.asp?cmd=logout">Log out</a></p>
<% Else %>
    <div align="center">
    <h2>HMS Licence Generator</h2>
    <form id="f" action="default.asp" method="post">
    <input type="hidden" name="cmd" value="login" />
      <table border="0">
        <tr>
          <td><b>Username</b></td>
          <td>
            <input type="text" name="username" value="<%=Request("username") %>" />
          </td>
        </tr>
        <tr>
          <td><b>Password</b></td>
          <td>
            <input type="password" name="password" />
          </td>
        </tr>
        <tr>
          <td align="right" colspan="2">
            <input type="submit" value="Login" name="submit" />
          </td>
        </tr>
        <tr>
          <td>
            <font size="2"><a href="admin.asp">Admin section</a></font>
          </td>
        </tr>
      </table>
    </form>
    </div>
<% End If %>

<% Call Destroy(con) %>

</body>
</html>
