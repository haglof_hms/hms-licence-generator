<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Session("admin") <> "true" Then Response.Redirect "admin.asp" %>

<html>
<body>

<%
Dim cmd, con, rst, rst2, SQL
Dim i, j, item, retailerid, retailername, moduleid, name, existing
Dim objLst, objXML

cmd = Request("cmd")

Call MakeConn(con)

Select Case cmd
Case "update"
    retailerid = FilterInput(Request("retailerid"))
    retailername = Trim(FilterInput(Request("retailername")))

    'Delete any existing permission for this retailer
    SQL = "DELETE FROM retailerpermission WHERE retailerid = " & retailerid
    Call con.Execute(SQL)

    'Add specified retailers for this retailer
    For Each item In Request.Form
        If Left(item, 4) = "mid_" And Request(item) = "on" Then
            moduleid = FilterInput(Mid(item, 5))
            SQL = "INSERT INTO retailerpermission (retailerid, moduleid) VALUES(" & retailerid & ", '" & moduleid & "')"
            Call con.Execute(SQL)
        End If
    Next

    'Update retailer name
    If retailername <> vbNullString Then
        SQL = "UPDATE retailer SET retailername = '" & retailername & "' WHERE retailerid = " & retailerid
        Call con.Execute(SQL)
    End If

    Call Destroy(con)
    Response.Redirect "retailers.asp"

Case "view"
    'Parse project file xml
    Set objXML = Server.CreateObject("Microsoft.XMLDOM")
    Set objLst = Server.CreateObject("Microsoft.XMLDOM")
    objXML.async = False
    objXML.Load(ProjectDir & "\" & ProjectFile)
    If objXML.parseError.errorCode <> 0 Then
        Response.Write "An error occured while trying to parse licence project file.<p>"
        Response.Write "Reason: " & objXML.parseError.reason & "<br>"
        Response.Write "Number: " & objXML.parseError.errorCode & "<br>"
        Response.Write "Line: " & objXML.parseError.line & "<br>"
        Response.End
    End If
    
    SQL = "SELECT retailername FROM retailer WHERE retailerid = " & FilterInput(Request("retailerid"))
    Call MakeRs_view(rst,con,SQL)
    retailername = rst("retailername")
    Call Destroy(rst)
    %>

    <form method="post" action="retailers.asp">
    <input type="hidden" name="cmd" value="update">
    <input type="hidden" name="retailerid" value="<%=Request("retailerid") %>">

    <b>Retailer name</b><br>
    <input type=text name="retailername" value="<%=retailername %>" style="width:200px">
    <p>

    <table border=0>
    <tr>
    <td></td>
    <td style="width:80px"><b>Id</b></td>
    <td style="width:auto"><b>Module</b></td>
    </tr>

    <%
    Set objLst = objXML.getElementsByTagName("Module")

    'List permission for this retailer
    For i = 0 To objLst.length - 1
        moduleid = vbNullString
        name = vbNullString
        existing = False

        For j = 0 To objLst.item(i).childNodes.Length - 1
            Select Case objLst.item(i).childNodes(j).nodeName
            Case "ModuleNumber", "ModuleID"
                moduleid = objLst.item(i).childNodes(j).text
            Case "ModuleName"
                name = objLst.item(i).childNodes(j).text
            End Select
        Next

        'Check if module exists in retailer's retailers
        SQL = "SELECT moduleId FROM retailerPermission WHERE moduleId = '" & FilterInput(moduleid) & "' AND retailerId = " & Request("retailerid")
        Call MakeRs_view(rst,con,SQL)
        If Not rst.Eof Then existing = True
        Call Destroy(rst)

        %>
        <tr>
        <td><input type="checkbox" name="mid_<%=moduleid %>" <%If existing = True Then Response.Write "checked"%>></td>
        <td><%=moduleid %></td>
        <td><%=name %></td>
        </tr>
        <%
    Next

    Set objLst = Nothing
    Set objXML = Nothing
    %>

    </table>
    <p><input type="submit" value="Update">
    </form>

    <%
Case Else
    %>

    <table border=0>
    <tr>
    <td><b>Retailer</b></td>
    <td><b>Permissions</b></td>
    </tr>

    <%
    'List retailers
    SQL = "SELECT * FROM retailer"
    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        %>
        <tr>
        <td><a href="retailers.asp?cmd=view&retailerid=<%=rst("retailerid") %>"><%=rst("retailername") %></a></td>
        <td>
        <%
        i = 0
        SQL = "SELECT * FROM retailerpermission WHERE retailerid = " & rst("retailerid") & " ORDER BY moduleid"
        Call MakeRs_view(rst2,con,SQL)
        Do Until rst2.Eof
            If i > 0 Then Response.Write ", "
            Response.Write rst2("moduleid")
            i = i + 1
            rst2.MoveNext
        Loop
        Call Destroy(rst2)
        %>
        </td>
        </tr>
        <%
        rst.MoveNext
    Loop
    Call Destroy(rst)
    %>

    </table>

    <%
End Select

Call Destroy(con)
%>

<p><a href="admin.asp">Back to main menu</a>

</body>
</html>
