<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Int(Session("retailerid")) <= 0 Then Response.Redirect "./" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title></title></head>
<body <%If Request("cmd") = vbNullString Then %>onload="document.getElementById('company').focus()"<%End If %>>

<%
Dim con, rst, SQL, i
Dim conditions, customerid, company, contactname

Call MakeConn(con)

Select Case Request("cmd")
Case "showresults"
    customerid  = Trim(FilterInput(Request("customerid")))
    company     = Trim(FilterInput(Request("company")))
    contactname = Trim(FilterInput(Request("contactname")))

    'Prepare conditions for query
    If IsNumeric(customerid) Then
        If conditions <> vbNullString Then condition = conditions & " AND "
        conditions = conditions & " customernum = " & customerid
    End If
    If company <> vbNullString Then
        If conditions <> vbNullString Then condition = conditions & " AND "
        conditions = conditions & " company LIKE '%" & Replace(company, " ", "%") & "%'"
    End If
    If contactname <> vbNullString Then
        If conditions <> vbNullString Then condition = conditions & " AND "
        conditions = conditions & " name LIKE '%" & contactname & "%'"
    End If

    'Make sure at least one search field were filled in
    If conditions = vbNullString Then Response.Redirect "customer-search.asp"

    %><table cellspacing="0"><%
    'List query results
    SQL = "SELECT * FROM customer WHERE " & conditions & " ORDER BY company, name, customernum"
    Call MakeRs_view(rst,con,SQL)
    If rst.Eof Then
        %><tr><td>No results.</td></tr><%
    End If
    Do Until rst.Eof
        %>
        <tr <%If i Mod 2 = 1 Then %>style="background-color:#cccccc"<%End If %>>
        <td style="width:50px"><%=rst("customernum") %></td>
        <%If Request("opt") = "addremove" Then %>
            <td style="width:250px"><a href="#" onclick="window.opener.location='customer-register.asp?cmd=viewedit&customerid=<%=rst("customernum") %>&name=<%=rst("name") %>'; return false;"><%=rst("company") %></a></td>
        <%Else %>
            <td style="width:250px"><a href="#" onclick="window.opener.document.getElementById('customernum').value='<%=rst("customernum") %>'; window.opener.document.getElementById('company').value='<%=rst("company") %>'; return false;"><%=rst("company") %></a></td>
        <%End If %>
        <td style="width:150px"><%=rst("name") %></td>
        <%If Request("opt") = "addremove" Then %>
            <td style="width:50px"><a href="#" onclick="window.opener.location='customer-register.asp?cmd=addcopy&customerid=<%=rst("customernum") %>&name=<%=rst("name") %>'; return false;">(add)</a></td>
            <td style="width:50px"><a href="customer-search.asp?cmd=remove&customerid=<%=rst("customernum") %>&name=<%=rst("name") %>">(remove)</a></td>
        <%End If %>
        </tr>
        <%
        i = i + 1
        rst.MoveNext
    Loop
    Call Destroy(rst)
    %></table><hr /><%

Case "remove"
    SQL = "DELETE FROM customer WHERE customernum = " & FilterInput(Request("customerid")) & " AND name LIKE '" & FilterInput(Request("name")) & "'"
    con.Execute(SQL)

    Call Destroy(con)
    Response.Redirect "customer-search.asp?cmd=done&opt=addremove"
End Select

Call Destroy(con)
%>

<%If Request("cmd") = "done" Then %>Done.<hr /><%End If %>
<form action="customer-search.asp" method="post">
<input type="hidden" name="cmd" value="showresults" />
<input type="hidden" name="opt" value="<%=Request("opt") %>" />
<table>
<tr><td>Company</td><td><input type="text" name="company" /></td></tr>
<tr><td>Contact name</td><td><input type="text" name="contactname" /></td></tr>
<tr><td>Customerno.</td><td><input type="text" name="customerid" /></td></tr>
<tr><td><input type="submit" value="Search" /></td></tr>
</table>
</form>

</body>
</html>
