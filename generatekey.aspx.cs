using System;

public partial class _Default : System.Web.UI.Page
{
    protected int exec(String outfile)
    {
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.EnableRaisingEvents = true;
        proc.StartInfo.WorkingDirectory = "C:\\Inetpub\\AdminScripts";
        proc.StartInfo.FileName = "alg26.exe";
        proc.StartInfo.Arguments = String.Concat("-o temp\\hms-order.xml -c temp\\hms-config.xml -xl temp\\HaglofManagementSystems.lic -xk temp\\", outfile, " -xml yes");
        proc.Start();
        proc.WaitForExit();
        return proc.ExitCode;
    }
}
