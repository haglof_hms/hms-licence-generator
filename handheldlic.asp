<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Int(Session("retailerid")) <= 0 And Session("supervisor") = "true" Then Response.Redirect "./" %>
<% Server.ScriptTimeout = 120 %>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>

<%
Dim i, cmd, item, orderid, error
Dim con, rst, rst2, SQL

Server.ScriptTimeout = 120 '2 minutes

cmd = Request("cmd")

Select Case cmd
Case "generate"
    'Check constraints
    If Not IsNumeric(Request("prg")) Or Request("prg") = vbNullString Or Request("disksn") = vbNullString Then
        error = "You must specify program and disk serial number."
    End If

    If error = vbNullString Then
        'Automatically store all form variable in session so we can obtain it when the licence key is generated
        For Each item In Request.QueryString
            Session(item) = Request.QueryString(item)
        Next

        Response.Redirect "hhgen.aspx?prg=" & Request("prg") & "&disksn=" & Request("disksn")
    End If

Case "post-generate"
    'Check return code for an error
    If Int(Request("retcode")) = 0 Then
        Response.Write "Licence generator failed."
        Response.End
    End If

    'Retrieve generated key
    Call MakeConn(con)

    'Store order data
    SQL = "SELECT TOP 1 * from licenceorder"
    Call MakeRs_add2(rst,con,SQL) 'MakeRs_add2 gives us a resyncable recordset
    rst.AddNew
    rst("date")             = now
    rst("copyprotection")   = 0
    rst("hwid")             = UCase(Session("disksn"))
    rst("userid")           = Session("userid")
    rst("ip")               = Request.ServerVariables("REMOTE_HOST")

    'Set customer number for this order if and only if a customer with this number exists
    If IsNumeric(Session("customernum")) Then
        SQL = "SELECT customernum FROM customer WHERE customernum = " & FilterInput(Session("customernum"))
        Call MakeRs_add(rst2,con,SQL)
        If Not rst2.Eof Then
            rst("customernum") = Session("customernum")
        End If
        Call Destroy(rst2)
    End If

    'Get order id of newly created record
    rst.Resync 'rst.Update
    orderid = rst("orderid")
    Call Destroy(rst)

    Select Case Int(Session("prg"))
    Case 1
        item = "PocketDP"
    Case 2
        item = "SDI"
    Case 3
        item = "ComLink"
    Case 4
        item = "Linjetax"
    Case 5
        item = "ArcPlots"
    Case 6
        item = "VMFCom"
	Case 7
		item = "MDIICom"
    Case Else
        item = "&lt;unknown module&gt;"
    End Select

    'Store generated key
    SQL = "SELECT TOP 1 * FROM licence"
    Call MakeRs_add(rst,con,SQL)
    rst.AddNew
    rst("orderid")      = orderid
    rst("modulename")   = item
    rst("moduleid")     = Session("prg")
    rst("licencekey")   = PadWithZeros(CStr(Hex(Request("retcode"))), 8)
    rst.Update
    Call Destroy(rst)
    Call Destroy(con)

    Response.Redirect "licence-view.asp?cmd=viewlast&id=" & orderid
End Select

If error <> vbNullString Then
    %><div align="center"><font color="red"><%=error %></font></div><hr /><%
End if

If cmd = vbNullString Or error <> vbNullString Then
    %>

    <form id="form1" method="get" action="handheldlic.asp">
    <input type="hidden" name="cmd" value="generate" />
    <table border="0">
    <tr><td style="width:90px">Program</td><td>
    <select name="prg" style="width:140px">
        <option value="1">PocketDP</option>
        <option value="2">SDI</option>
        <option value="3">ComLink</option>
        <option value="4">Linjetax</option>
        <option value="5">ArcPlots</option>
        <option value="6">VMFCom</option>
		<option value="7">MDIICom</option>
    </select>
    </td></tr>
    <tr><td>Disk serial#</td><td><input type="text" name="disksn" maxlength="8" style="width:140px" value="<%=Request("serial") %>" /></td></tr>
    <tr style="height:40px" valign="bottom"><td>Customerno.</td><td><input type="text" name="customernum" value="<%=Request("customernum") %>" /></td></tr>
    <tr><td colspan="2"><a href="#" onclick="window.open('customer-search.asp', 'customerreg', 'height=600,width=450,resizable=0,scrollbars=1')"><font size="2">Search customer register</font></a></td></tr>
    </table>

    <p><input type="submit" value="Create licence" /></p>
    </form>

    <%
End If

Function PadWithZeros(num, ct)
    Do Until Len(num) >= ct
        num = "0" & num
    Loop

    PadWithZeros = num
End Function
%>

<p><a href="./">Back to main menu</a></p>
 
</body>
</html>
