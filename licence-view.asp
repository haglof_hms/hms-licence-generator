<% Option Explicit %>
<!-- #INCLUDE FILE="include/adovbs.inc" -->
<!-- #INCLUDE FILE="include/functions.inc" -->
<!-- #INCLUDE FILE="include/defines.inc" -->

<% If Int(Session("retailerid")) <= 0 Then Response.Redirect "./" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
.lic:hover {
  background-color     : #acb2ff;
  color                : #000000;
  cursor               : pointer;
}
</style>

<script type="text/javascript" language="javascript">
function ClipBoard(text) 
{
  holdtext.innerText = text;
  Copied = holdtext.createTextRange();
  Copied.execCommand("Copy");
}
</script>
</head>
<body <%If LCase(Request("licencefile")) = "true" Then %>onload="window.location.href='licence-view.asp?cmd=downloadlic'"<%End If %>>

<% If Request("cmd") = "viewlast" Or Request("cmd") = "showresults" Or Request("cmd") = vbNullString Then %>
    <table cellpadding="0" cellspacing="0">
    <tr>
    <%If Session("admin") = "true" Then %><td></td><%End If %>
    <td style="width:100px"><b>Id</b></td>
    <td style="width:200px"><b>Module</b></td>
    <td style="width:300px"><b>Customer</b></td>
    <td style="width:150px"><b><%If Request("cmd") <> "viewlast" Then %><a href="licence-view.asp?orderby=hwid&opt=<%=Request("opt") %>&noact=<%=Request("noact") %>"><%End If %>Hardware ID<%If Request("cmd") <> "viewlast" Then %></a><%End If %></b></td>
    <td style="width:400px"><b>Licence key</b></td>
    <td style="width:200px"><b><%If Request("cmd") <> "viewlast" Then %><a href="licence-view.asp?orderby=date&opt=<%=Request("opt") %>&noact=<%=Request("noact") %>"><%End If %> Order date<%If Request("cmd") <> "viewlast" Then %></a><%End If %></b></td>
    <td style="width:200px"><b>Retailer</b></td>
    <td style="width:100px"><b>User</b></td>
    </tr>
<% End If %>

<%
Dim con, rst, SQL, condition, sort
Dim item, i, viewAll, text
Dim j, lastOrderId
Dim fso

Call MakeConn(con)

If Request("cmd") = "viewlast" Then
    If Not IsNumeric(Request("id")) Or Trim(Request("id")) = vbNullString Then Response.Redirect "licence-view.asp"

    SQL = "SELECT DISTINCT modulename, moduleid, licencekey, date, copyprotection, hwid, company, useraccount.username, retailername " & _
          "FROM licenceorder " & _
          "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
          "INNER JOIN licence ON licence.orderid = licenceorder.orderid " & _
          "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
          "INNER JOIN retailer ON retailer.retailerid = " & Session("retailerid") & " " & _
          "WHERE licence.orderid = " & FilterInput(Request("id")) & " " & _
          "ORDER BY date DESC"
    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        %>
        <tr <%If i Mod 2 = 1 Then%>bgcolor="#cccccc"<%End If%>>
        <td><%=rst("moduleid") %></td>
        <td><%=rst("modulename") %></td>
        <td><%=rst("company") %></td>
        <td><%=rst("hwid") %> (<%=rst("copyprotection") %>)</td>
        <td><%=rst("licencekey") %></td>
        <td><%=rst("date") %></td>
        <td><%=rst("retailername") %></td>
        <td><%=rst("username") %></td>
        </tr>
        <%
        text = text & rst("modulename") & "\t" & rst("licencekey") & "\n"
        i = i + 1
        rst.MoveNext
    Loop
    Call Destroy(rst)
    %>
    <tr style="height:40px" valign="bottom"><td>
    <textarea id="holdtext" style="display:none;"></textarea>
    <button onclick="ClipBoard('<%=text %>');">Copy to Clipboard</button>
    </td></tr>
    <tr style="height:40px" valign="bottom"><td><a href="licence-view.asp">History</a></td></tr>
    <%
ElseIf Request("cmd") = "downloadlic" Then
    'Download licence file (we need to let the client redirect here so the viewlast response shows up before streaming binary data)
    DownloadFile(TempDir + "\" + "HaglofManagementSystems.lic")

    Set fso = Server.CreateObject("Scripting.FileSystemObject")
    fso.DeleteFile(TempDir + "\" + "HaglofManagementSystems.lic")
    Set fso = Nothing
    Response.End
ElseIf Request("cmd") = "search" Then
    %>
    <form method="post" action="licence-view.asp">
    <input type="hidden" name="cmd" value="showresults" />
    <table>
    <tr><td colspan="2"><b>Search licences</b></td></tr>
    <tr><td>Name</td><td><input type="text" name="name" /></td></tr>
    <tr><td>Company</td><td><input type="text" name="company" /></td></tr>
    <tr><td>Notes</td><td><input type="text" name="notes" /></td></tr>
    <tr><td>Module</td><td><input type="text" name="module" /></td></tr>
    <tr><td>Hardware ID</td><td><input type="text" name="hwid" /></td></tr>
    <tr><td>Date</td><td><input type="text" name="date_from" /></td><td>-</td><td><input type=text name="date_to"></td></tr>
    <tr><td colspan="2"><input type="checkbox" name="showact" />&nbsp;Show activation keys</td></tr>
    <% If Session("supervisor") = "true" Then %><tr><td colspan=2><input type="checkbox" name="onlymine" />&nbsp;Only my licences</td></tr><% End If %>
    <tr><td><input type="submit" value="Search" /></td></tr>
    </table>
    </form>
    <%
ElseIf Request("cmd") = "remove" Then
    For Each item In Request.Form
        If Left(item, 5) = "order" Then
            i = Mid(item, 6)
            If IsNumeric(i) And Trim(i) <> vbNullString Then
                SQL = "DELETE FROM licenceorder WHERE orderid = " & FilterInput(i)
                con.Execute(SQL)
            End If
        End If
    Next

    Call Destroy(con)
    Response.Redirect "licence-view.asp?noact=" & Request("noact") & "&opt=" & Request("opt") & "&orderby=" & Request("orderby")
Else
    'Show activation keys on/off
    If Request("noact") = "true" Or (Request("showact") <> "on" And Request("cmd") = "showresults") Then
        condition = " moduleid <> '1' "
    Else
        condition = " 1 = 1 " 'Dummy value (always true)
    End If

    viewAll = False
    If Request("cmd") = "showresults" Then
        'Put together query based on search criteria
        If Request("name") <> vbNullString Then
            condition = condition & " AND name LIKE '%" & FilterInput(Request("name")) & "%'"
        End If
        If Request("company") <> vbNullString Then
            condition = condition & " AND company LIKE '%" & FilterInput(Request("company")) & "%'"
        End If
        If Request("notes") <> vbNullString Then
            condition = condition & " AND notes LIKE '%" & FilterInput(Request("notes")) & "%'"
        End If
        If Request("module") <> vbNullString Then
            condition = condition & " AND modulename LIKE '%" & FilterInput(Request("module")) & "%'"
        End If
        If Request("hwid") <> vbNullString Then
            condition = condition & " AND hwid LIKE '" & FilterInput(Request("hwid")) & "%'"
        End If
        If IsDate(Request("date_from")) Then
            condition = condition & " AND date > '" & FilterInput(Request("date_from")) & "'"
        End If
        If IsDate(Request("date_to")) Then
            condition = condition & " AND date < '" & FilterInput(Request("date_to")) & "'"
        End If
        
        'View only my/all licences
        If Request("onlymine") <> "on" And Session("supervisor") = "true" Then
            viewAll = True
        End If
    End If

    If Request("orderby") = "hwid" Then
        sort = "hwid DESC, date DESC, retailername, username"
    Else
        sort = "date DESC, retailername, username"
    End If

    If Request("opt") = "all" And Session("supervisor") = "true" Or viewAll = True Then
        'Select date from all retailers
        SQL = "SELECT DISTINCT licence.orderid, moduleid, modulename, " & _
                "licenceorder.customernum, company, " & _
                "licencekey, date, copyprotection, hwid, useraccount.username, retailername " & _
              "FROM licenceorder " & _
              "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
              "INNER JOIN licence ON licence.orderid = licenceorder.orderid " & _
              "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
              "INNER JOIN retailer ON retailer.retailerid = useraccount.retailerid " & _
              "WHERE " & condition & " " & _
              "ORDER BY " & sort
    Else
        'Select data from all users under the same retailer as current user
        SQL = "SELECT DISTINCT licenceorder.orderid, moduleid, modulename, " &_
                "licenceorder.customernum, company, " & _
                "licencekey, date, copyprotection, hwid, useraccount.username, retailername " & _
              "FROM licenceorder " & _
              "LEFT JOIN customer ON customer.customernum = licenceorder.customernum " & _
              "INNER JOIN licence ON licence.orderid = licenceorder.orderid " & _
              "INNER JOIN useraccount ON useraccount.userid = licenceorder.userid " & _
              "INNER JOIN retailer ON retailer.retailerid = useraccount.retailerid " & _
              "WHERE EXISTS(" & _
                "SELECT * FROM useraccount WHERE EXISTS(" & _
                    "SELECT * FROM retailer WHERE EXISTS (" & _
                        "SELECT * FROM useraccount WHERE userid = " & Session("userid") & " AND retailer.retailerid = useraccount.retailerid) " & _
                    "AND useraccount.retailerid = retailer.retailerid) " & _
                "AND licenceorder.userid = useraccount.userid) " & _
              "AND " & condition & " " & _
              "ORDER BY " & sort
    End If
    
    If Session("admin") = "true" Then
        %>
        <form action="licence-view.asp" method="post">
        <input type="hidden" name="cmd" value="remove" />
        <input type="hidden" name="opt" value="<%=Request("opt") %>" />
        <input type="hidden" name="noact" value="<%=Request("noact") %>" />
        <input type="hidden" name="orderby" value="<%=Request("orderby") %>" />
        <%
    End If

    Call MakeRs_view(rst,con,SQL)
    Do Until rst.Eof
        If Session("admin") = "true" Then
            If rst("orderid") <> lastOrderId Then j = j + 1
            %>
            <tr <%If j Mod 2 = 0 Then%>bgcolor="#cccccc"<%End If%>>
            <td><%If rst("orderid") <> lastOrderId Then %><input name="order<%=rst("orderid") %>" type="checkbox" /><%End If %></td>
        <%Else %>
            <tr class="lic" <%If i Mod 2 = 1 Then%>bgcolor="#cccccc"<%End If%> onclick="window.location.href='details.asp?id=<%=rst("orderid") %>&selection=customer'">
        <%End If %>
        <td><%=rst("moduleid") %></td>
        <td><%=rst("modulename") %></td>
        <td><a style="color:#000000" href="details.asp?id=<%=rst("orderid") %>&selection=customer"><%=rst("company") %></a></td>
        <td><a style="color:#000000" href="details.asp?id=<%=rst("orderid") %>&selection=hwid"><%=rst("hwid") %> (<%=rst("copyprotection") %>)</a></td>
        <td><%=rst("licencekey") %></td>
        <td><%=rst("date") %></td>
        <td><%=rst("retailername") %></td>
        <td><%=rst("username") %></td>
        </tr>
        <%
        lastOrderId = rst("orderid")
	    i = i + 1
        rst.MoveNext
    Loop

    If Session("admin") = "true" Then %>
        <tr><td style="height:30px" valign="bottom" colspan="2"><input type="submit" value="Remove" /></td></tr>
    <%End If %>

    <tr style="height:30px" valign="bottom"><td colspan="2"><b><%=rst.RecordCount %> licences</b></td></tr>
    <%
    Call Destroy(rst)
End If

Call Destroy(con)
%>

</table>

<% If Request("cmd") = vbNullString Then %>
    <p>
    <% If Session("supervisor") = "true" Then %>
        <% If Request("opt") = "all" Then %>
            <a href="licence-view.asp?noact=<%=Request("noact") %>&orderby=<%=Request("orderby") %>">View my licences</a>
        <% Else %>
            <a href="licence-view.asp?opt=all&noact=<%=Request("noact") %>&orderby=<%=Request("orderby") %>">View all licences</a>
        <% End If %>
        &nbsp;/&nbsp;
    <% End If %>

    <% If Request("noact") = "true" Then %>
        <a href="licence-view.asp?opt=<%=Request("opt") %>&orderby=<%=Request("orderby") %>">Show activation keys</a>
    <% Else %>
        <a href="licence-view.asp?noact=true&opt=<%=Request("opt") %>&orderby=<%=Request("orderby") %>">Hide activation keys</a>
    <% End If %>

    <br /><a href="licence-view.asp?cmd=search">Search</a>
    </p>
<% End If %>

<p><a href="./">Back to main menu</a></p>

</body>
</html>
